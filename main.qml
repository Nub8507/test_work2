import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.13

Window {
    id: window

    visible: true
    title: "Lines"
    width: 540
    height: 590
    flags: Qt.WindowCloseButtonHint | Qt.CustomizeWindowHint | Qt.Dialog | Qt.WindowTitleHint

    function calkColor(num){
        if (num === 1)
            return "red.png"
        else if (num === 2)
            return "green.png"
        else if (num === 3)
            return "blue.png"
        else if (num === 4)
            return "black.png"
        else
            return "empty.png"
    }

    property int game_scope: 0

    Connections {
        target: cpp_table_model
        function onSetEnabled(enable) {
            tableView.enabled=enable
        }
        function onSetScope(scope) {
            game_scope=scope
        }
    }

    Rectangle {
        id: rect_head
        width: parent.width
        height: 46
        color: "#ffffff"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        Button {
            id: button_new
            text: qsTr("New game")
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 10
            clip: true
            onClicked: cpp_table_model.newGame()
        }

        Label {
            id: label
            text: qsTr("Scope:")
            anchors.right: text_scope.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 10
            font.pixelSize: 16
        }

        Text {
            id: text_scope
            text: game_scope
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 10
            font.pixelSize: 14
            Behavior on text {
                SequentialAnimation {
                    NumberAnimation {
                        target: text_scope
                        property: "scale"
                        duration: 200
                        to: 0
                        easing.type: Easing["InQuad"]
                    }
                    PropertyAction { }
                    NumberAnimation {
                        target: text_scope
                        property: "scale"
                        duration: 200
                        to: 1
                        easing.type: Easing["OutQuad"]
                    }
                }

            }
        }

    }
    Rectangle {
        id: rect_table
        color: "#c5ffcd"
        anchors.top: rect_head.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 3

        TableView {
            id: tableView
            anchors.fill: parent

            model: cpp_table_model

            enabled: false

            columnSpacing: 2.5
            rowSpacing: 2.5

            interactive: false

            columnWidthProvider: function( column ) {
                return 57;
            }

            rowHeightProvider: function( row ){
                return 57;
            }


            delegate: Item {
                implicitWidth:  image.width
                implicitHeight: image.height
                required property int cellColor
                required property bool is_empty
                required property int cell_index
                required property bool cell_selected

                SequentialAnimation{
                    id: anim_blink
                    running: cell_selected
                    loops: Animation.Infinite
                    PropertyAnimation {
                        target: image
                        property: "scale"
                        duration: 200
                        to: 0.7
                        easing.type: Easing["InQuad"]
                    }
                    PropertyAnimation {
                        target: image
                        property: "scale"
                        duration: 200
                        to: 1
                        easing.type: Easing["OutQuad"]
                    }
                    PropertyAction { }
                }
                //
                    Image {
                        id: image
                        sourceSize.height: 100
                        sourceSize.width: 100
                        anchors.fill: parent
                        source: calkColor(cellColor)
                        scale: 1
                        opacity: is_empty ? 0 : 1
                        Behavior on opacity {
                            SequentialAnimation {
                                NumberAnimation {duration: 500}
                                PropertyAction { }
                            }
                        }
                    }
                MouseArea {
                    id: mouseArea
                    width: image.width
                    height: image.height
                    onClicked: {
                        cpp_table_model.mouseClick(cell_index)
                    }
                }
                onCell_selectedChanged: {
                    image.scale = 1
                }

            }
        }
    }





}
