#include "mainboard.h"
#include <QRandomGenerator>

MainBoard::MainBoard(int x,int y,QObject *parent)
    : QAbstractItemModel(parent), size_x(x), size_y(y)
{
    table_data=QVector<tableNode>(size_x*size_y);
    selected=-1;
    scope=0;
    enabled=true;
    //
    data_storage=nullptr;
    //
    data_storage=new sqLiteData();
    data_storage->moveToThread(&sql_thread);
    connect(&sql_thread, &QThread::finished, data_storage, &QObject::deleteLater);
    connect(data_storage,&sqLiteData::loadCells,this,&MainBoard::loadCells,Qt::QueuedConnection);
    connect(data_storage,&sqLiteData::loadScope,this,&MainBoard::loadScope,Qt::QueuedConnection);
    sql_thread.start();
    QMetaObject::invokeMethod(data_storage,"connectToDatabase",Qt::QueuedConnection);
    //
}

MainBoard::~MainBoard()
{
    sql_thread.quit();
    sql_thread.wait();
}

QModelIndex MainBoard::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if (row < 0 || column < 0)
        return QModelIndex();
    return createIndex(row, column, nullptr);
}

QModelIndex MainBoard::parent(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return QModelIndex();
}

int MainBoard::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return size_y;
}

int MainBoard::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return size_x;
}

QVariant MainBoard::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    //
    auto col=index.column();
    auto row=index.row();
    //
    if(col>=size_x||row>=size_y)
        return QVariant();
    //
    auto &node=dataFromTable(row,col);
    if(role==cellColor){
        return static_cast<int>(node.color);
    }else if(role==cellFilling){
        return node.is_empty;
    }else if(role==cellIndex){
        return indexFromTable(index.row(),index.column());
    }else if(role==cellSelected){
        return node.selected;
    }
    return QVariant();
}

QHash<int, QByteArray> MainBoard::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[cellColor] = "cellColor";
    roles[cellFilling] = "is_empty";
    roles[cellIndex] = "cell_index";
    roles[cellSelected] = "cell_selected";
    return roles;
}

void MainBoard::cpuTurn()
{
    //
    for(int i=0;i<3;i++){
        if(ballsCount()<(size_x*size_y)){
            checkLines(addRndBall());
        }else{
            stop();
            return;
        }
    }
    saveData();
    //
}

void MainBoard::mouseClick(int index)
{
    //
    if(index>size_x*size_y-1)return;
    //
    auto &node=table_data[index];
    if(node.is_empty==false){
        if(selected==-1){
            node.selected=true;
            selected=index;
            updateCell(index);
        }else{
            table_data[selected].selected=false;
            updateCell(selected);
            node.selected=true;
            selected=index;
            updateCell(index);
        }
    }else{
        if(selected!=-1){
            table_data[selected].selected=false;
            std::swap(table_data[selected],node);
            updateCell(index);
            updateCell(selected);
            selected=-1;
            if(checkLines(index)){
                cpuTurn();
            }else{
                saveData();
            }
        }
    }
    //
}

int MainBoard::addRndBall()
{
    //
    int ind=static_cast<int>(QRandomGenerator::global()->generate()%(static_cast<unsigned int>(size_x*size_y)));

    while(!table_data[ind].is_empty){
        ind++;
        if(ind>size_x*size_y-1)
            ind=0;
    }
    table_data[ind].is_empty=false;
    table_data[ind].color=static_cast<cellColors>(QRandomGenerator::global()->generate()%4+1);
    updateCell(ind);
    return ind;
    //
}

int MainBoard::ballsCount()
{
    //
    int balls_num=0;
    for(const auto &cell:table_data){
        if(!cell.is_empty)
            balls_num++;
    }
    return balls_num;
    //
}

MainBoard::tableNode &MainBoard::dataFromTable(int row, int col)
{
    //
    return table_data[row*size_x+col];
    //
}

const MainBoard::tableNode &MainBoard::dataFromTable(int row, int col) const
{
    //
    return table_data[row*size_x+col];
    //
}

int MainBoard::indexFromTable(int row, int col) const
{
    //
    if(row<0||row>size_y-1||col<0||col>size_x-1)return -1;
    return row*size_x+col;
    //
}

QPair<int, int> MainBoard::tableFromIndex(int index) const
{
    //
    if(index>size_x*size_y-1||index<0)return {-1,-1};
    return {index/size_x,index%size_x};
    //
}

void MainBoard::updateCell(int index)
{
    //
    auto coord=tableFromIndex(index);
    auto ball_index=MainBoard::index(coord.first,coord.second);
    emit dataChanged(ball_index,ball_index);
    //
}

bool MainBoard::checkLines(int index)
{
    //
    bool rez=true;
    if(index>size_x*size_y-1)return rez;
    //
    auto top=checkNextCell(table_data[index].color,index,-1,0);
    auto bottom=checkNextCell(table_data[index].color,index,1,0);
    auto left=checkNextCell(table_data[index].color,index,0,-1);
    auto right=checkNextCell(table_data[index].color,index,0,1);
    //
    if((top.size()+bottom.size()-1)>=5){
        for(auto n_index: qAsConst(top)){
            table_data[n_index].is_empty = true;
            table_data[n_index].color = colorEmpty;
            updateCell(n_index);
        }
        for(auto n_index: qAsConst(bottom)){
            table_data[n_index].is_empty = true;
            table_data[n_index].color = colorEmpty;
            updateCell(n_index);
        }
        scope+=10;
        QMetaObject::invokeMethod(data_storage,"saveScope",Qt::QueuedConnection,Q_ARG(int,scope));
        emit setScope(scope);
        rez=false;
    }
    //
    if((left.size()+right.size()-1)>=5){
        for(auto n_index: qAsConst(left)){
            table_data[n_index].is_empty = true;
            table_data[n_index].color = colorEmpty;
            updateCell(n_index);
        }
        for(auto n_index: qAsConst(right)){
            table_data[n_index].is_empty = true;
            table_data[n_index].color = colorEmpty;
            updateCell(n_index);
        }
        scope+=10;
        QMetaObject::invokeMethod(data_storage,"saveScope",Qt::QueuedConnection,Q_ARG(int,scope));
        emit setScope(scope);
        rez=false;
    }
    return rez;
    //
}

QVector<int> MainBoard::checkNextCell(MainBoard::cellColors color, int index, int row_offset, int col_offset)
{
    //
    QVector<int> rez;
    if(index!=-1){
        if(!table_data[index].is_empty && (table_data[index].color == color) ){
            rez.push_back(index);
            auto new_coord=tableFromIndex(index);
            new_coord.first+=row_offset;
            new_coord.second+=col_offset;
            auto new_index=indexFromTable(new_coord.first,new_coord.second);
            rez.append(checkNextCell(color,new_index,row_offset,col_offset));
        }
    }
    return rez;
    //
}

void MainBoard::saveData()
{
    //
    QVector<int> data;
    //
    for(int i=0;i<table_data.size();++i){
        int t=i*100;
        t+=table_data[i].color*10;
        t+=static_cast<int>(table_data[i].is_empty);
        data.append(t);
    }
    //
    QMetaObject::invokeMethod(data_storage,"saveCells",Qt::QueuedConnection,Q_ARG(QVector<int>,data));
    //
}

void MainBoard::stop()
{
    //
    for(int i=0;i<table_data.size();++i){
        if(table_data[i].selected){
            table_data[i].selected=false;
            updateCell(i);
        }
    }
    emit setEnabled(false);
    //
}

void MainBoard::startGame()
{
    //
    QMetaObject::invokeMethod(data_storage,"getScope",Qt::QueuedConnection);
    QMetaObject::invokeMethod(data_storage,"getCells",Qt::QueuedConnection);
    //
}

void MainBoard::newGame()
{
    //
    for(int n_index=0;n_index<table_data.size();++n_index){
        table_data[n_index].color=colorEmpty;
        table_data[n_index].is_empty=true;
        updateCell(n_index);
    }
    scope=0;
    //
    QMetaObject::invokeMethod(data_storage,"saveScope",Qt::QueuedConnection,Q_ARG(int,scope));
    emit setScope(scope);
    //
    cpuTurn();
    emit setEnabled(true);
    //
}

void MainBoard::loadCells(QVector<int> data)
{
    //
    if(data.size()>0){
        //
        for(auto &val:table_data){
            val.is_empty=true;
        }
        //
        auto max_index=size_x*size_y-1;
        //
        for(const auto &val:data){
            int ind=val/100;
            int col=(val/10)%10;
            int en=val%10;
            //
            if(ind<0||ind>max_index)continue;
            //
            table_data[ind].color=static_cast<cellColors>(col);
            table_data[ind].is_empty= en ? true: false;
            //
        }
        //
        auto index_first=MainBoard::index(0,0);
        auto index_last=MainBoard::index(size_x-1,size_y-1);
        emit dataChanged(index_first,index_last);
    }
    //
    if(ballsCount()==0){
        cpuTurn();
        emit setEnabled(true);
    }else if(ballsCount()<(size_x*size_y)){
        emit setEnabled(true);
    }else{
        emit setEnabled(false);
    }
    //
}

void MainBoard::loadScope(int scope)
{
    //
    this->scope=scope;
    emit setScope(scope);
    //
}
