#ifndef SQLITEDATA_H
#define SQLITEDATA_H

#include <QObject>
#include <QSqlDatabase>
#include <QString>
#include <QVector>

class sqLiteData : public QObject
{
    Q_OBJECT
public:
    explicit sqLiteData(QObject *parent = nullptr);

signals:
    void loadCells(QVector<int> data);
    void loadScope(int scope);

public slots:

    bool connectToDatabase();
    void getCells();
    void getScope();
    bool saveScope(int scope);
    bool saveCells(QVector<int> data);

private:
    const QString base_hostname="localhost";
    const QString base_name="data.db";
    const QString table_scope="scope";
    const QString table_data="data";

    const QString scope_field="scope";
    const QString cell_index_field="cell_index";
    const QString cell_color_field="cell_color";
    const QString cell_enabled_field="cell_enabled";


    QSqlDatabase db;
    bool enabled;

    bool openDataBase();
    bool restoreDataBase();
    void closeDataBase();
    bool createTable();
};

#endif // SQLITEDATA_H
