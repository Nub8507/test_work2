#ifndef MAINBOARD_H
#define MAINBOARD_H

#include <QAbstractItemModel>
#include <QThread>
#include <sqlitedata.h>

class MainBoard : public QAbstractItemModel
{
    Q_OBJECT

public:

    enum cellsRoles{
        cellColor = Qt::UserRole + 1,
        cellFilling,
        cellIndex,
        cellSelected,

    };

    bool enabled;

    explicit MainBoard(int x,int y,QObject *parent = nullptr);
    ~MainBoard();

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void cpuTurn();
    Q_INVOKABLE void newGame();
    Q_INVOKABLE void mouseClick(int index);

signals:
    void setEnabled(bool enable);
    void setScope(int scope);

public slots:
    void startGame();    

private slots:
    void loadCells(QVector<int> data);
    void loadScope(int scope);

private:

    enum cellColors{
            colorEmpty      =   0,
            colorRed,
            colorGreen,
            colorBlue,
            colorBlack,
    };


    struct tableNode{
        cellColors color;
        bool is_empty;
        bool selected;

        tableNode(){
            color = colorEmpty;
            is_empty = true;
            selected = false;
        }
    };

    const int size_x;
    const int size_y;

    QVector<tableNode> table_data;
    int selected;
    int scope;
    QThread sql_thread;
    sqLiteData *data_storage;


    int addRndBall();
    int ballsCount();
    tableNode &dataFromTable(int row,int col);
    const tableNode &dataFromTable(int row,int col) const;
    int indexFromTable(int row,int col) const;
    QPair<int,int> tableFromIndex(int index) const;
    void updateCell(int index);
    bool checkLines(int index);
    QVector<int> checkNextCell(cellColors color, int index, int row_offset, int col_offset);

    void saveData();
    void stop();

};

#endif // MAINBOARD_H
