#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>


#include "mainboard.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    MainBoard model(9,9);


    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("cpp_table_model", QVariant::fromValue(&model));
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);
    //
    QMetaObject::invokeMethod(&model,"startGame",Qt::QueuedConnection);

    return app.exec();
}
