#include "sqlitedata.h"

#include <QFile>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

sqLiteData::sqLiteData(QObject *parent)
    : QObject{parent}
{    
    enabled=false;
}

bool sqLiteData::connectToDatabase()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    //
    if(!QFile(base_name).exists()){
        enabled = this->restoreDataBase();
    } else {
        enabled = this->openDataBase();
    }
    return enabled;
}

void sqLiteData::getCells()
{
    QVector<int> rez;
    if(!enabled){
        emit loadCells(rez);
        return;
    }
    //
    QSqlQuery query("SELECT * from " + table_data);
    while(query.next()){
        int t=0;
        t=query.value(2).toInt();
        t+=query.value(1).toInt()*10;
        t+=query.value(0).toInt()*100;
        rez.append(t);
    }
    emit loadCells(rez);
}

void sqLiteData::getScope()
{
    //
    if(!enabled) return;
    //
    QSqlQuery query("SELECT * from " + table_scope);
    if(query.next()){
        emit loadScope(query.value(0).toInt());
    }
    //
}

bool sqLiteData::saveScope(int scope)
{
    //
    if(!enabled) return false;
    //
    QSqlQuery query;
    bool rez=false;
    rez = query.exec("UPDATE " + table_scope + " SET " +
                            scope_field + " = " + QString::number(scope));
    //
    return rez;
    //
}

bool sqLiteData::saveCells(QVector<int> data)
{
    //
    if(data.size()==0) return true;
    //
    QSqlQuery query;
    bool rez=false;
    //INSERT OR REPLACE INTO data(cell_index,cell_color,cell_enabled) VALUES (2,1,1),(3,1,0),(4,2,0),(5,1,0);
    QString query_string="INSERT OR REPLACE INTO ";
    query_string+=table_data + " ( ";
    query_string+=cell_index_field+", ";
    query_string+=cell_color_field+", ";
    query_string+=cell_enabled_field+" ) VALUES ";
    //
    for(const auto val:data){
        int ind=val/100;
        int col=(val/10)%10;
        int en=val%10;
        QString s=" ( "+QString::number(ind)+",";
        s+=QString::number(col)+",";
        s+=QString::number(en)+"),";
        query_string+=s;
    }
    //
    query_string=query_string.left(query_string.size()-1);
    //
    rez = query.exec(query_string);
    //
    return rez;
    //
}

bool sqLiteData::openDataBase()
{
    //
    db.setDatabaseName(base_name);
    auto rez= db.open();
    return rez;
    //
}

bool sqLiteData::restoreDataBase()
{
    if(this->openDataBase()){
        return this->createTable();
    } else {
        return false;
    }
    return false;
}

void sqLiteData::closeDataBase()
{
    //
    db.close();
    //
}

bool sqLiteData::createTable()
{
    //
    QSqlQuery query;
    bool rez=false;
    rez = query.exec("CREATE TABLE IF NOT EXISTS " + table_scope + " (" +
                            scope_field + " INTEGER )");
    if(rez){
        rez &= query.exec("CREATE TABLE IF NOT EXISTS " + table_data + " (" +
                                cell_index_field + " INTEGER UNIQUE," +
                                cell_color_field + " INTEGER," +
                                cell_enabled_field + " INTEGER)");

    }
    //
    if(rez){
        rez &= query.exec("INSERT INTO " + table_scope + " (" +
                                scope_field + " ) VALUES (0)");

    }
    //
    return rez;
    //
}

